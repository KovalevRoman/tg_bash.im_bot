# Телеграм бот-цитатник Bash.im  #

### Доступные комманды ###
* /start - поддерживаемые команды
* /random, /q - случайная цитата
* /q [pharse] - поиск phrase в цитатах и вывод наденной цитаты

Разработка ботов на заказ: 

Email: rvk.sft[_at_]gmail.com

TG: @ValeriiTopor

------------------------------------------------------------------------------
### Telegram messenger bot ###
* Deliver quotes from http://bash.im

### Supported commands ###
* /start - help information about supporting commands
* /random, /q - deliver random quote
* /q [pharse] - search pharase in quotes, select and deliver the quote

TG bot developing: @ValeriiTopor
Email: rvk.sft[_at_]gmail.com