
import re
import telebot
import config
import requests
from bs4 import BeautifulSoup
import random
import urllib.parse





bot = telebot.TeleBot(config.token)

def get_mes(word):
    base_url = 'http://bash.im/index?text='
    random_url = 'http://bash.im/random'
    if word == 'random':
        req = requests.get(random_url)
    else:
        word = urllib.parse.urlencode({'text':word}, encoding='cp1251')
        base_url = re.sub('text=', '', base_url)
        print(base_url+word)
        req = requests.get(base_url+word)
    soup = BeautifulSoup(req.text, "html.parser")
    res = soup.find_all('div', class_="quote")
    rand = random.randint(0, len(res))
    res = res[rand].find('div', class_="text")
    for br in res.find_all("br"):
        br.replace_with("\n")
    return (res.text)


@bot.message_handler(commands=['start', 'help'])
def handle_start_help(message):
    hello_mes = 'Здравствуйте\nВведите команду /random, чтобыполучить случайную цитату\nИли "/q фраза", где фраза - должна присутствовать в цитате '
    bot.send_message(message.chat.id, hello_mes)

@bot.message_handler(commands=['random'])
def handle_start_help(message):
    mes = get_mes('random')
    bot.send_message(message.chat.id, mes)

@bot.message_handler(commands=['q'])
def handle_start_help(message):
    a = re.sub(r'/q ', '', message.text)
    if a == r'/q':
        a = 'random'
    mes = get_mes(a)
    bot.send_message(message.chat.id, mes)

'''
@bot.message_handler(commands=[ 'wrk', 'list', 'cmd' ])
def handle_list(message):
    if message.chat.id < 0:
        bot.send_message(message.chat.id, 'Please use private messages')
        return
    text = '\U0001f1f7\U0001f1fa /freelansim -- Last job from freelansim.ru\n' + \
           '\U0001f1f7\U0001f1fa /freelancehunt -- Last job from freelansim.ru\n' + \
           '\U0001f1fa\U0001f1f8 /freelancecom -- Last job from freelance.com'

    bot.send_message(message.chat.id, text)


@bot.message_handler(commands=[ 'stats', 'st' ])
def handle_stats(message):
    text = 'Statistics by category of job'
    text = text + '\n' + '``` Time            ( 1   /7   /30 days)```'
    categories = [ 'admin', 'webdev', 'dev', 'webdis' ]
    for category in categories:
        # print(category)
        day, week, month = get_stats_by(category)
        text = text + '\n' + '``` Jobs in {:8s}: {:,d}  {:,d}  {:,d}```'.format(category, day, week, month)
        # print(text)

    print(text)
    bot.send_message(message.chat.id, text, parse_mode='MARKDOWN')

@bot.message_handler(commands=[ 'freelancecom', 'fc' ])
def handle_freelancecom(message):
    if message.chat.id < 0:
        bot.send_message(message.chat.id, 'Please use private messages')
        return
    output = '\u2328 /freelance_adm - Last jobs for sysadmins\n' + \
             '\u2692 /freelance_webdev - Last jobs for Web Developers\n' + \
             '\U0001f307 /freelance_webdis - Last jobs for Web Designers\n' + \
             '\U0001f6e0 /freelance_dev - Last jobs for Developers'
    bot.send_message(message.chat.id, output)
'''

if __name__ == '__main__':
    pass
    # get_mes('random')
    print("The Bot Владимир Started")
    bot.polling(none_stop=True)
    # bot.polling(none_stop=False)